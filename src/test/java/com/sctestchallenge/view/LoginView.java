/* 
 * 
 * LoginView.java
 * soundcloud-test-challenge
 * 
 * Created by Steve Chun on 16 Jun 2015
 * 
 */
package com.sctestchallenge.view;
import com.sctestchallenge.common.AndroidDriverManager;
import com.sctestchallenge.utils.CommonUtil;

/**
 * Page object for Login View
 */
public class LoginView extends CommonUtil {
	
	public LoginView(final AndroidDriverManager manager) {
		super(manager);		
	}

	public LoginView enterUsernameAndPassword(final String username, final String password) {
		findElementByTypeAtIndex("EditText", 0).sendKeys(username);
		getDriver().hideKeyboard();
	    findElementByTypeAtIndex("EditText", 1).sendKeys(password);
	    getDriver().hideKeyboard();
	    return performLogin();
	}
	
	private LoginView performLogin() {
		findElementByTypeAtIndex("Button", 0).click();
		return this;
	}
	
}

