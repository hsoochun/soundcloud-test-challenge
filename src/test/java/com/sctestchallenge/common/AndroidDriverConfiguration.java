/* 
 * 
 * AndroidDriverConfiguration.java
 * soundcloud-test-challenge
 * 
 * Created by Steve Chun on 16 Jun 2015
 * 
 */
package com.sctestchallenge.common;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

public class AndroidDriverConfiguration {

	private static TestConfiguration testConfig = new TestConfiguration();

	/**
	 * Create WebDriver based on the configuration of test properties
	 * 
	 * @return WebDriver
	 */
	public AppiumDriver create() {
		AppiumDriver driver = initialiseDriverWithDesiredCapabilities();
		return driver;
	}

	private AppiumDriver initialiseDriverWithDesiredCapabilities() {
		AppiumDriver driver = getAppiumAndroidDriver(getAndroidDesiredCapabilities());
		return driver;
	}
	
	private static AppiumDriver getAppiumAndroidDriver(final DesiredCapabilities desiredCapabilities) {
		URL url;
		try {
			url = new URL(String.format("http://%s:%d/wd/hub",
					testConfig.getAppiumHost(), testConfig.getAppiumPort()));
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
		return new AndroidDriver(url, desiredCapabilities);
	}
	
	private static DesiredCapabilities getAndroidDesiredCapabilities() {
		File app = new File(testConfig.getAppPath());
        
        final DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION,
				testConfig.getMobPlatformVersion());
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,
        		testConfig.getDevice());
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION,
        		testConfig.getMobPlatformVersion());
        capabilities.setCapability(MobileCapabilityType.APP,
        		app.getAbsolutePath());
        capabilities.setCapability(MobileCapabilityType.APP_PACKAGE,
        		testConfig.getAppPackage());
        capabilities.setCapability("noReset", testConfig.getResetOption());
        capabilities.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY,
        		"LoginActivity, LikesActivity");
        capabilities.setCapability(MobileCapabilityType.APP_ACTIVITY,
        		"MainActivity");
        return capabilities;
	}

}
