/* 
 * 
 * AndroidDriverManager.java
 * soundcloud-test-challenge
 * 
 * Created by Steve Chun on 16 Jun 2015
 * 
 */
package com.sctestchallenge.common;

import io.appium.java_client.AppiumDriver;

/**
 * Class to manage the driver instance
 */
public class AndroidDriverManager {
	
	private final CustomAppiumDriver DRIVER;
	
	public AndroidDriverManager(final CustomAppiumDriver driver) {
		this.DRIVER = driver;
	}
	
	public AppiumDriver getDriver() {
		return this.DRIVER.getDriver();
	}
	
	public void cleanDriver() {
		this.DRIVER.manage().deleteAllCookies();
	}
	
}

