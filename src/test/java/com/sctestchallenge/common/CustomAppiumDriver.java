/* 
 * 
 * CustomAppiumDriver.java
 * soundcloud-test-challenge
 * 
 * Created by Steve Chun on 16 Jun 2015
 * 
 */
package com.sctestchallenge.common;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/**
 * Class to manage (initialize and kill) the appium driver
 */
public class CustomAppiumDriver extends EventFiringWebDriver {
	
	private static AppiumDriver APPIUM_DRIVER;
	
	public CustomAppiumDriver(final AndroidDriverConfiguration wdConfig) {
		super(initialiseAppiumDriver(wdConfig));
	}
	
	private static WebDriver initialiseAppiumDriver(final AndroidDriverConfiguration wdConfig) {
		if (APPIUM_DRIVER == null) {
			APPIUM_DRIVER = wdConfig.create();
		}
		return APPIUM_DRIVER;
	}
	
	// Shutdown the application after test is completed
	static {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (APPIUM_DRIVER != null) {
					APPIUM_DRIVER.quit();
				}
			}
		});
	}
	
	public AppiumDriver getDriver() {
		return APPIUM_DRIVER;
	}
	
}

