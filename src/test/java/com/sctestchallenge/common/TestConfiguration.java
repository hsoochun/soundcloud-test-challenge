/* 
 * 
 * TestConfiguration.java
 * soundcloud-test-challenge
 * 
 * Created by Steve Chun on 16 Jun 2015
 * 
 */
package com.sctestchallenge.common;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * Contains properties for test execution
 */
public class TestConfiguration {
	
	Properties props = new Properties();

	public TestConfiguration() {
		try {
			final URL url = TestConfiguration.class.getClassLoader()
					.getResource("test.properties");
			this.props.load(url.openStream());
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public String getAppiumHost() {
		return this.props.getProperty("appium.host");
	}
	
	public Integer getAppiumPort() {
		return Integer.valueOf(this.props.getProperty("appium.port"));
	}

	public String getScreenshotDirectory() {
		return this.props.getProperty("screenshot.dir");
	}
	
	public String getScreenshotDirectoryForFailed() {
		return this.props.getProperty("screenshot.dir.failed");
	}
	
	public String getMobPlatform() {
		return this.props.getProperty("mob.platform");
	}
	
	public String getMobPlatformVersion() {
		return this.props.getProperty("mob.platform.ver");
	}
	
	public Boolean getResetOption() {
		return Boolean.parseBoolean(this.props.getProperty("mob.test.noReset"));
	}
	
	public Boolean getTestRunOnSimulator() {
		return Boolean.parseBoolean(this.props.getProperty("mob.test.simulator"));
	}
	
	public String getDevice() {
		return this.props.getProperty("mob.test.device");
	}

	public String getAppPath() {
		return this.props.getProperty("mob.test.app.path");
	}
	
	public String getAppPackage() {
		return this.props.getProperty("mob.test.app.package");
	}

}

