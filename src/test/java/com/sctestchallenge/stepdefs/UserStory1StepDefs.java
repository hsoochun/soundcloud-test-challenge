/* 
 * 
 * UserStory1StepDefs.java
 * soundcloud-test-challenge
 * 
 * Created by Steve Chun on 16 Jun 2015
 * 
 */
package com.sctestchallenge.stepdefs;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import com.sctestchallenge.common.AndroidDriverManager;
import com.sctestchallenge.view.LoginView;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UserStory1StepDefs {

	private final LoginView loginView;

	public UserStory1StepDefs(final AndroidDriverManager manager) {
		this.loginView = new LoginView(manager);
	}

	/*******************************************************************
	 * GIVEN
	 *******************************************************************/
	
	@Given("^login page is opened$")
	public void login_page_is_opened() throws Throwable {
		if (this.loginView.findElement("LogIn") == null) {
			this.loginView.findElement("More options").click();
			this.loginView.findElement("Log Out").click();
			if (this.loginView.findElement("LogIn") == null) {
				this.loginView.getDriver().navigate().back();
			}
			assertThat("Login page is not displayed",
		    		this.loginView.findElementByType("Button").getText(), equalTo("LogIn"));
		}
	}
	
	/*******************************************************************
	 * WHEN
	 *******************************************************************/

	@When("^user signs in with invalid username$")
	public void user_signs_in_with_invalid_username() throws Throwable {
		this.loginView.enterUsernameAndPassword("abcde", "password");
	}

	@When("^user signs in with invalid password$")
	public void user_signs_in_with_invalid_password() throws Throwable {
		this.loginView.enterUsernameAndPassword("test-user@sc.com", "abcde");
	}
	
	@When("^user logs in successfully$")
	public void user_logs_in_successfully() throws Throwable {
		this.loginView.enterUsernameAndPassword("test-user@sc.com", "password");
	}
	
	/*******************************************************************
	 * THEN
	 *******************************************************************/

	@Then("^\"(.*?)\" dialog is displayed$")
	public void dialog_is_displayed(final String message) throws Throwable {
	    assertThat("Alert with the title is not displayed",
	    		this.loginView.findAlertByTitle(message), not(equalTo(null)));
	    this.loginView.findElement("OK").click();
	}
	
	@Then("^likes list is displayed$")
	public void likes_list_is_displayed() throws Throwable {
	    assertThat("My Likes view is not displayed",
	    		this.loginView.findElementByXPath("//android.view.View[1]/android.widget.FrameLayout[1]"
		    		+ "/android.view.View[1]/android.widget.LinearLayout[1]"
		    		+ "/android.widget.LinearLayout[1]/android.widget.TextView[1]").getText(),
		    		equalTo("My Likes"));
	}
	
}

