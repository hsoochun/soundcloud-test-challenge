/* 
 * 
 * RunCucumberTest.java
 * soundcloud-test-challenge
 * 
 * Created by Steve Chun on 16 Jun 2015
 * 
 */
package com.sctestchallenge;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * Cucumber test runner class with configurable options
 */
@RunWith(Cucumber.class)
@CucumberOptions(tags = { "~@wip", "~@manual" },
format = { "html:target/reports", "usage:target/usage.json" },
monochrome = true, strict = false)

public class RunCucumberTest {

}

