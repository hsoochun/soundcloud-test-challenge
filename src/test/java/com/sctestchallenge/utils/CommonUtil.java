/* 
 * 
 * CommonUtil.java
 * soundcloud-test-challenge
 * 
 * Created by Steve Chun on 16 Jun 2015
 * 
 */
package com.sctestchallenge.utils;

import io.appium.java_client.AppiumDriver;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.sctestchallenge.common.AndroidDriverManager;

public class CommonUtil {
	
	protected final AndroidDriverManager manager;
	
	public CommonUtil(final AndroidDriverManager manager) {
		this.manager = manager;
	}
	
	public AppiumDriver getDriver() {
		return this.manager.getDriver();
	}
	
	// Find Mobile Elements
	public WebElement findElementByType(final String elementType) {
		return getDriver().findElementByClassName("android.widget." + elementType);
	}
	
	public List<WebElement> findElementsByType(final String elementType) {
		return getDriver().findElementsByClassName("android.widget." + elementType);
	}
	
	public WebElement findElementByTypeAtIndex(final String elementType, final int index) {
		List<WebElement> elements = findElementsByType(elementType);
		return elements.get(index);
	}
	
	public WebElement findElementByXPath(final String value) {
		try {
			return getDriver().findElementByXPath(value);
		} catch (NoSuchElementException e) {
			return null;
		}
	}
	
	public WebElement findElement(final String value) {
		try {
			return getDriver().findElementByXPath("//*[@content-desc=\"" + value + "\" or @resource-id=\"" + value +
					"\" or @text=\"" + value + "\"] | //*[contains(translate(@content-desc,\"" + value +
					"\",\"" + value + "\"), \"" + value + "\") or contains(translate(@text,\"" + value +
					"\",\"" + value + "\"), \"" + value + "\") or @resource-id=\"" + value + "\"]");
		} catch (NoSuchElementException e) {
			return null;
		}
	}
	
	public WebElement findAlertByTitle(final String alertTitle) {
		try {
			return getDriver().findElementByXPath("//*[@resource-id=\"android:id/alertTitle\" and @text=\""
					+ alertTitle + "\"]");
		} catch (NoSuchElementException e) {
			return null;
		}
	}
	
	public void waitForResponse(final int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
