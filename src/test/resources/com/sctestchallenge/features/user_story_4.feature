@wip
Feature: User Story 4
  As a User 
  I would like see my liked track in a player  
  So that I can play my tracks

  Background: 
    Given likes page is opened

  Scenario: clicking on a LikesList opens a track's player page
    When a liked track is tapped
    Then the player page for the track is displayed

  Scenario: player page shows the clicked track's title + icon + name and artwork
    When the player page for a track is displayed
    Then the following information of the track is displayed on the player page:
      | title of the track | icon | name | artwork |

  Scenario Outline: user can swipe to the next and previous tracks in the player
    When the player page for a track is displayed
    And swipe the page to <DIRECTION>
    Then the <TRACK_ORDER> track's player page is displayed

    Examples: 
      | DIRECTION | TRACK_ORDER |
      | left      | next        |
      | right     | previous    |

  Scenario: user can go back to the Likes list
    When the player page for a track is displayed
    And user taps back button
    Then likes page is displayed
