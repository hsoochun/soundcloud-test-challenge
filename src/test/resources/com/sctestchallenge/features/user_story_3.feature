@wip
Feature: User Story 3
  As a User 
  I would like to see my likes 
  So that I can quickly access my favourite content

  Background: 
    Given user is logged in

  Scenario: likes list has title and contains 25 liked tracks with no pagination
    Then likes page is displayed
    And the page has title "My Likes"
    And the likes list contains 25 liked tracks
    And the likes list doesn't paginate upon scrolling to the bottom of the list

  Scenario: list item contains Artist name and track name + image
    Then likes page is displayed
    And each liked track contains the following information:
      | artist name | track name | image |
