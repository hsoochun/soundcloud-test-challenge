Feature: User Story 1
  As a User 
  I would like to log into the application 
  So that I start to personalize my experience

  Scenario Outline: display "Couldn't log you in" dialog when user signs in with invalid username/password
    Given login page is opened
    When user signs in with invalid <FIELD>
    Then "Couldn't log you in" dialog is displayed

    Examples: 
      | FIELD    |
      | username |
      | password |

  Scenario: show likes list upon successful login
    Given login page is opened
    When user logs in successfully
    Then likes list is displayed

  @manual
  Scenario: show valid email mark in the username text field if user enters properly formatted email
    Given login page is opened
    When user enters properly formatted email address in the username field
    Then valid email mark is displayed in the field

  @manual
  Scenario: show invalid email mark if user types in non-standard email
    Given login page is opened
    When user enters non-standard formatted email address in the username field
    Then invalid email mark is displayed in the field

  @manual
  Scenario: restarting the app does not sign the user out
    Given user is logged in
    When user restarts the app
    Then the user is not signed out
