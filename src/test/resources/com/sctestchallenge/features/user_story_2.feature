@wip
Feature: User Story 2
  As a User 
  I would like to sign out 
  So that other users can use the app without seeing my content

  Background: 
    Given user is logged in

  Scenario: user can sign out in the settings menu
    When user signs out in the settings menu
    Then login page is displayed

  Scenario: login screen should be shown after user sign out
    When user taps back button
    Then the user is not signed out
    And login page is not displayed

  @manual
  Scenario: login screen should be shown after user sign out
    When user signs out in the settings menu
    Then the access token is removed and then login page is displayed
