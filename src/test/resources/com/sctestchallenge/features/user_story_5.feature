@wip
Feature: User Story 5
  As a User 
  I would like to play the track 
  So that I can enjoy the experience

  Background: 
    Given player page of a track is opened

  Scenario: toggle button that changes to "playing" state
    Given the track is in 'stopped' state
    When play button is tapped
    Then the track becomes in 'playing' state

  Scenario: toggle button that changes to "stopped" state
    Given the track is in 'playing' state
    When pause button is tapped
    Then the track becomes in 'paused' state

  @manual
  Scenario: track is not played when the track is in "playing" state
    When the track is in 'playing' state
    Then the track is not played
