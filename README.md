# SoundCloud Test Challenge #

This is source code of SoundCloud Test Challenge for Test Engineer position.

### Requirements ###

* Java 7
* Maven
* Android SDK

### Configuration ###

* Make sure all the fields in `/src/test/resources/test.properties` file is set

### Usage ###

* This automated test is using Appium with Cucumber.
* This test is made for execution on actual device. It may run on emulator but may require extra configuration.
* You can run test by `mvn clean test` OR run 'RunCucumberTest.java' as `JUnit Test` in Eclipse.

### Continuous Integration ###

* Use custom shell script to customize test.properties file.
* Make sure your application is build on the target before executing the test with Maven.

### Tags in Feature Files ###

* `@wip`: Work in progress
** These are the scenarios which requires automation scripts to be written.
* `@manual`: Manual testing required
** These are the scenarios which requires manual testing (fully or partially).

### Test Cases ###

* Test cases are replace by Cucumber test scenario.
** Manual exploratory testing is required for the test scenarios with tag `@manual`, as these tests will not be executed (will be ignored) during the automated test run.

### Conditions and Assumptions ###

* Only tested on Mac OS 10.10.3, on Nexus 6 with Android 5.1.1.
* Test is not fully written. This is only an example.
* Most of strings are hardcoded for now. It can be separated and managed.

### Challenge Summary ###
* Time spend: about 8 hours